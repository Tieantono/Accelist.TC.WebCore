﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Accelist.TC.ReCoreWebApp.Models;
using Accelist.TC.ReCoreWebApp.Services;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Accelist.TC.ReCoreWebApp.Controllers
{
    [AutoValidateAntiforgeryToken]
    public class VideoGameController : Controller
    {
        public VideoGameController(VideoGameService videoGameService)
        {
            this.VideoGameService = videoGameService;
        }

        private readonly VideoGameService VideoGameService;

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Submit(VideoGameViewModel videoGame)
        {
            if(ModelState.IsValid == false)
            {
                return View("Index");
            }
            await VideoGameService.Create(videoGame);
            return RedirectToAction("Detail");
        }

        [HttpGet]
        public async Task<IActionResult> Detail(int id)
        {
            var videoGame = await VideoGameService.GetVideoGameById(id);
            return View(videoGame);
        }

        [HttpGet]
        public async Task<IActionResult> GetImage(int id)
        {
            return File(await VideoGameService.GetImage(id), "image/png");
        }
    }
}
