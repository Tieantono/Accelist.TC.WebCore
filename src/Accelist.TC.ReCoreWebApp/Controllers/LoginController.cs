﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Accelist.TC.ReCoreWebApp.Models;
using Accelist.TC.ReCoreWebApp.Services;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Accelist.TC.ReCoreWebApp.Controllers
{
    public class LoginController : Controller
    {
        public LoginController(RepoService repoService, List<Login> logins)
        {
            this.RepoService = repoService;
            this.Login = new Login();
            this.Logins = logins;
        }
        private readonly RepoService RepoService;
        private readonly Login Login;
        private readonly List<Login> Logins;

        [HttpGet]
        public IActionResult Index()
        {
            RepoService.Add(new Login
            {
                Username = "admin",
                Password = "admin123"
            });
            this.Logins.Add(new Login
            {
                Username = "admin",
                Password = "admin123"
            });
            return View();
        }

        [HttpGet]
        public IActionResult Result(Login login)
        {
            return View(login);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index(Login login)
        {
            if(ModelState.IsValid == false)
            {
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Result", login);
        }
    }
}
