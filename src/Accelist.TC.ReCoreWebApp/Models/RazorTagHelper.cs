﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.Runtime.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Accelist.TC.ReCoreWebApp.Models
{
    [HtmlTargetElement("calculator")]
    public class RazorTagHelper : TagHelper
    {
        [HtmlAttributeName("first-number")]
        public int FirstNumber { get; set; }

        [HtmlAttributeName("second-number")]
        public int SecondNumber { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var result = this.FirstNumber + this.SecondNumber;
            output.Content.AppendHtml($"<p>Your number: { result }</p>");
        }
    }
}
