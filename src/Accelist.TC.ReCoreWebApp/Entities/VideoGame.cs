﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accelist.TC.ReCoreWebApp.Entities
{
    public class VideoGame
    {
        [Key]
        public int VideoGameId { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public byte[] Image { get; set; }
    }
}
