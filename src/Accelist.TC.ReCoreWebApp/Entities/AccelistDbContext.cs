﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Accelist.TC.ReCoreWebApp.Entities
{
    public class AccelistDbContext: DbContext
    {
        public AccelistDbContext(DbContextOptions<AccelistDbContext> options): base(options) { }

        public DbSet<VideoGame> VideoGame { get; set; }
    }
}
