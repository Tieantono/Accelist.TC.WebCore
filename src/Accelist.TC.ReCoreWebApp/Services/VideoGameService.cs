﻿using Accelist.TC.ReCoreWebApp.Entities;
using Accelist.TC.ReCoreWebApp.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Accelist.TC.ReCoreWebApp.Services
{
    public class VideoGameService
    {
        public VideoGameService(AccelistDbContext accelistDbContext)
        {
            this.AccelistDbContext = accelistDbContext;
        }

        private readonly AccelistDbContext AccelistDbContext;

        public async Task Create(VideoGameViewModel videoGame)
        {
            using(var ms = new MemoryStream())
            {
                await videoGame.Picture.CopyToAsync(ms);

                var newVideoGame = new VideoGame {
                    Name = videoGame.Name,
                    Price = videoGame.Price,
                    Image = ms.ToArray()
                };

                AccelistDbContext.Add(newVideoGame);
                await AccelistDbContext.SaveChangesAsync();
            }
        }

        public async Task<VideoGameViewModel> GetVideoGameById(int id)
        {
            return await AccelistDbContext.VideoGame.Where(Q => Q.VideoGameId == id)
                .Select(Q => new VideoGameViewModel
                {
                    VideoGameId = Q.VideoGameId,
                    Name = Q.Name,
                    Price = Q.Price
                })
                .FirstOrDefaultAsync();
        }

        public async Task<byte[]> GetImage(int id)
        {
            return await AccelistDbContext.VideoGame
                .Where(Q => Q.VideoGameId == id)
                .Select(Q => Q.Image)
                .FirstOrDefaultAsync();
        }
    }
}
