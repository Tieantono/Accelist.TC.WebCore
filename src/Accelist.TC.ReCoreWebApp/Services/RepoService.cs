﻿using Accelist.TC.ReCoreWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accelist.TC.ReCoreWebApp.Services
{
    public class RepoService
    {
        public RepoService()
        {
            Logins = new List<Login>();
        }
        private readonly List<Login> Logins;

        public void Add(Login login)
        {
            this.Logins.Add(login);
        }
    }
}
